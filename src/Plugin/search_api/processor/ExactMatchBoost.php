<?php

namespace Drupal\search_api_exactmatch_boost\Plugin\search_api\processor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\search_api\Item\Item;
use Drupal\search_api\Plugin\PluginFormTrait;
use Drupal\search_api\Plugin\search_api\data_type\value\TextValue;
use Drupal\search_api\Processor\FieldsProcessorPluginBase;
use Drupal\search_api\Query\ResultSetInterface;
use Drupal\search_api\SearchApiException;
use Drupal\search_api\Query\QueryInterface;

/**
 * Puts elements that exactly match to the top of the results list.
 *
 * This processor puts elements that exactly match the text in a string index
 * field to the top of the results list. If a pager is used there maybe more
 * items on the first page and less items on following pages. Exact matches are
 * added to the first page, and removed from the regular result set afterwards.
 * If they appear in following pages they will be removed there.
 *
 * @SearchApiProcessor(
 *   id = "exactmatchboost",
 *   label = @Translation("Exact match boosting"),
 *   description = @Translation("Puts elements that exactly match to the top of the results list. Will only run on string index fields. Pager count may differ."),
 *   stages = {
 *     "postprocess_query" = 0,
 *   }
 * )
 */
class ExactMatchBoost extends FieldsProcessorPluginBase implements PluginFormInterface {

  use PluginFormTrait;

  /**
   * Search server instance.
   *
   * @var \Drupal\search_api\ServerInterface|null
   */
  protected $searchServer;

  /**
   * Transliteration processor if activated.
   *
   * @var \Drupal\search_api\Plugin\search_api\processor\Transliteration
   */
  protected $transliterationProcessor;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    try {
      $this->searchServer = $this->getIndex()->getServerInstance();
    }
    catch (SearchApiException $e) {
      $this->searchServer = FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'remove_exacts' => FALSE,
      'disable_full_processing_level' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    if ($this->searchServer) {
      $form['exakt_match_boost'] = [
        'hint' => [
          '#type' => 'markup',
          '#markup' => '<div class="messages messages--warning">' . $this->t('Exact match will only work on the fields listed below.') . '</div>',
        ],
      ];
      $form = parent::buildConfigurationForm($form, $form_state);

      $form['remove_exacts'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Remove exact matches from results.'),
        '#description' => $this->t('Remove the exact matches that are put on top from the results. This may cause problems or does not work for field types other than string.'),
        '#default_value' => $this->getConfiguration()['remove_exacts'],
      ];

      if ($this->searchServer->getBackendId() !== 'search_api_db') {
        $form['exakt_match_boost']['hint_paged'] = [
          '#type' => 'markup',
          '#markup' => '<div class="messages messages--warning">' . $this->t('On paged displays only first page will be affected. Best used on non paged display.') . '</div>',
        ];
      }

      $form['disable_full_processing_level'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Disable exact match boosting on the full processing level.'),
        '#description' => $this->t('Exact boost can slow down the search, it can be useful to disable it to speed up the search in specific context like autocomplete for example.'),
        '#default_value' => $this->getConfiguration()['disable_full_processing_level'],
      ];

      // Limit configuration of fields to textual fields only.
      $form['all_fields']['#access'] = FALSE;
      $fields = $this->getIndex()->getFields();
      $allowed_field_types = ['text', 'string', 'solr_text_custom'];
      foreach ($fields as $field) {
        [$field_type] = explode(':', $field->getType());
        if (!in_array($field_type, $allowed_field_types)) {
          unset($form['fields']['#options'][$field->getFieldIdentifier()]);
        }
        if ($field_type !== 'string') {
          if (empty($form['exakt_match_boost']['hint_paged'])) {
            $form['exakt_match_boost']['hint_paged'] =
              [
                '#type' => 'markup',
                '#markup' => '<div class="messages messages--warning">' . $this->t('Non-string fields will only affect first page of a paged display. Best used with non-paged displays.') . '</div>',
              ];
          }
        }
      }
    }
    else {
      $form['exakt_match_boost'] = [
        'hint' => [
          '#type' => 'markup',
          '#markup' => '<div class="messages messages--warning">' . $this->t('You need to add this index to a server first') . '</div>',
        ],
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $configuration = $this->getConfiguration();
    $value_fields = $values['fields'] ?? [];
    $configuration_fields = $configuration['fields'] ?? [];
    if (!empty(array_diff($value_fields, $configuration_fields))) {
      $form_state->set('processors_changed', TRUE);
    }
    $this->setConfiguration($values);
  }

  /**
   * {@inheritdoc}
   */
  public function postprocessSearchResults(ResultSetInterface $results) {
    $changeresult = FALSE;

    $query = $results->getQuery();

    if ($this->configuration['disable_full_processing_level'] === TRUE && !$results->getResultCount()
      || $query->getProcessingLevel() != QueryInterface::PROCESSING_FULL) {
      return;
    }

    $searchkeys = trim($query->getOriginalKeys(), '"');
    if (!empty($searchkeys)) {
      /** @var \Drupal\views\ViewExecutable $view */
      $view = $query->getOption('search_api_view');
      $exact_matches = [];
      try {
        $this->transliterationProcessor = $this->index->getProcessor('transliteration');
      }
      catch (SearchApiException $e) {
        $this->transliterationProcessor = FALSE;
      }
      foreach ($this->configuration['fields'] as $field) {
        if (!empty($field)) {
          if ($this->transliterationProcessor) {
            $translitConfig = $this->transliterationProcessor->getConfiguration();

            if (in_array($field, $translitConfig['fields'])) {
              $searchkeys = $this->transliterationProcessor->getTransliterator()
                ->transliterate($searchkeys, $this->transliterationProcessor->getLangcode());
            }
          }
          $current_field_exact_matches = $this->searchServer->getBackendId() == 'search_api_db' && $this->getIndex()->getField($field)->getType() === 'string' ? $this->getDatabaseExactMatches($results->getResultItems(), $searchkeys, $field) : $this->getGenericExactMatches($results->getResultItems(), $searchkeys, $field);
          $exact_matches = array_merge($current_field_exact_matches, $exact_matches);
        }
      }
      if (!empty($exact_matches)) {
        $changeresult = TRUE;
        $oldresults = $results->getResultItems();
        if (empty($view) || $view->usePager() === FALSE || $view->getCurrentPage() === 0) {
          $newresults = array_merge($exact_matches, $oldresults);

          // Seems that mini paged results includes the first result item of next page as last item of the results array
          // If this is an exact match, the real last result item (second last) might get lost, therefore
          // adding the last result item in this case again.
          if (!empty($view) && $view->usePager() && $view->getPager()->getPluginId() === 'mini') {
            $last = end($oldresults);
            $last_new = end($newresults);
            if ($last_new !== $last) {
              $newresults[] = $last;
            }
          }
        }
        else {
          // Remove exact match from pages other than first page.
          // Caution: only works reliable on string fields.
          if ($this->configuration['remove_exacts'] === TRUE) {
            foreach ($exact_matches as $key => $item) {
              unset($oldresults[$key]);
            }
          }
          $newresults = $oldresults;
        }
      }
    }
    if ($changeresult) {
      $results->setResultItems($newresults);
    }
    parent::postprocessSearchResults($results);
  }

  /**
   * Get exact matches for search_api_db.
   *
   * This searches the whole db for exact matches, even if they are not within
   * the first page of results.
   *
   * @param array|\Drupal\search_api\Item\ItemInterface[] $result_items
   *   The result items to search.
   * @param string $search_keys
   *   The search keys to match against.
   * @param string $field
   *   The field to search against.
   *
   * @return array|\Drupal\search_api\Item\ItemInterface[]
   *   List of exact matches.
   *
   * @throws \Drupal\search_api\SearchApiException
   */
  protected function getDatabaseExactMatches(array $result_items, string $search_keys, string $field): array {
    $backend = $this->searchServer->getBackend();
    $database = $backend->getDatabase();
    $exact_matches = [];
    $dbtable = $this->searchServer->getBackendId() . '_' . $this->index->id() . '_' . $field;
    $dbquery = $database->select($dbtable, 'dt');
    $dbquery->condition('dt.value', $search_keys);
    $dbquery->fields('dt', ['item_id']);
    $dbresults = $dbquery->execute();
    foreach ($dbresults as $dbresult) {
      $exact_matches[$dbresult->item_id] = new Item($this->index, $dbresult->item_id);
    }
    return $exact_matches;
  }

  /**
   * Search for exact matches to boost on non-db backends.
   *
   * This searches only the result set of the current page for exact matches.
   *
   * @param array|\Drupal\search_api\Item\ItemInterface[] $result_items
   *   The result items to search.
   * @param string $search_keys
   *   The search keys to match against.
   * @param string $field
   *   The field to search against.
   *
   * @return array|\Drupal\search_api\Item\ItemInterface[]
   *   List of exact matches.
   */
  protected function getGenericExactMatches(array $result_items, string $search_keys, string $field): array {
    $exact_matches = [];
    foreach ($result_items as $result_item_key => $result_item) {
      $result_item_field = $result_item->getField($field);
      $result_item_field_values = $result_item_field->getValues();
      if ($result_item_field_values) {
        $result_item_field_value = $result_item_field_values[0];
        if ($result_item_field_value instanceof TextValue) {
          $result_item_field_value = $result_item_field_value->getText();
        }
        if (is_string($result_item_field_value)) {
          if (trim(strtolower($result_item_field_value)) === trim(strtolower($search_keys))) {
            $exact_matches[$result_item_key] = $result_item;
          }
        }
      }
    }
    return $exact_matches;
  }

}
