This module allows you to boost search results that do exactly match the search keys on specific indexed fields. The exact matches will be put on top of the list.

This module is especially helpful if using partial matching for the search, but still want results to get higher ranked (by adding relevance), that do exactly match the search keys in an indexed field (i. e. title field).

This module is implemented as an Search API postprocess query processor plugin. You can configure on which fields (only string fields) of the index the processor acts. It works by adding the exact match to the top of the search results and by removing them in their regular place.

## Remarks / Limitations
This module might not correctly work with the Results summary views plugin, since it might add elements to one page and remove from another page resulting in difference of the counters. While this module does in principal work with paged queries, it might result in adding items on one page of the list and removing items on a later page.

### Solr backend (backend other than Search API DB)
In a solr setup this module will not work correctly with paged displays (only working on the first page). When using solr you might also consider to not use this module and maybe find a native way to do this with solr, see https://stackoverflow.com/questions/29103155/solr-exact-match-boost-over-text-containing-the-exact-match for reference.

## Installation

    Enable the module as usual
    Then go to admin/config/search/search-api/index/[index-name]/processors
    enable the "Exact match boosting" processor
    configure the processor and save configuration

## Usage example:

You have search results with title "Tooth", "Toothpaste" and "Tooth decay".

Now you are searching for "Tooth" with partial matching.

This will result in all three results getting the same relevancy for their title matching.

Now if you want to have the result for "Tooth" appear on top of the list, this is where this module comes in.